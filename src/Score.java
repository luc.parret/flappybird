
import java.net.URISyntaxException;
import java.util.Scanner;
        import java.io.File;
        import java.io.IOException;
        import java.io.PrintWriter;
        import java.io.FileNotFoundException;
        import java.io.UnsupportedEncodingException;


public class Score {

    // Read / Write to file setup
    private static final String FILE_PATH = null;
    private static File dataFile          = null;
    private static Scanner dataScanner    = null;
    private static PrintWriter dataWriter = null;

    // Highscore
    private int bestScore;
    public Score () {

        // Load scanner with data file
        try {
            //FILE_PATH = getClass().getResource("res/data/highscore.dat").toURI();
            dataFile = new File(getClass().getResource("res/data/highscore.dat").toURI());
            System.out.println(dataFile);
            dataScanner = new Scanner(dataFile);
        } catch (IOException | URISyntaxException e) {
            System.out.println(dataFile);
            System.out.println("Cannot load highscore!");
        }
        // Store highscore
        if (dataScanner.hasNextLine()) {
            bestScore = Integer.parseInt(dataScanner.nextLine());
        }


    }

    /**
     * @return     Player's highscore
     */
    public int bestScore () {
        return bestScore;
    }

    /**
     * Sets new highscore in the data file
     *
     * @param newBest     New score update
     */
    public void setNewBest (int newBest) {
        // Set new best score
        bestScore = newBest;
        try {
            // Write new highscore to data file
            dataWriter = new PrintWriter(dataFile, "UTF-8");
            dataWriter.println(Integer.toString(newBest));
            dataWriter.close();
        } catch (FileNotFoundException | UnsupportedEncodingException e) {
            System.out.println("Could not set new highscore!");
        }
    }
}
