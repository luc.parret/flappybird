import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class Main extends JFrame implements ActionListener{

    Ihm game;
    Timer gameTimer;

    // Game setup constants
    public static final int WIDTH  = 375;
    public static final int HEIGHT = 667;
    private static final int DELAY = 12;


    public Main() {

        super("Flappy Bird");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(WIDTH, HEIGHT);

        // Game timer
        gameTimer = new Timer(DELAY, this);
        gameTimer.start();

        // Add Panel to Frame
        game = new Ihm();
        add(game);

        // Set game icon
        setIconImage(Toolkit.getDefaultToolkit().getImage("res/img/icon.png"));

        setResizable(false);
        setVisible(true);
    }



    public static void main(String[] args) {
        Main game = new Main();

    }


    @Override
    public void actionPerformed(ActionEvent e) {
        if (game != null && game.ready) {
            game.repaint();
        }
    }
}
